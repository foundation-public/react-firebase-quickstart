//this was moved here as it was causing an error on hot reload
//when placed in the main file

import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyCyXY14A84LidZBMOpLHOQ7ZHWNLlngUmY",
    authDomain: "react-quick-staging.firebaseapp.com",
    databaseURL: "https://react-quick-staging.firebaseio.com",
    storageBucket: "react-quick-staging.appspot.com",
}
const app = firebase.initializeApp(config)

export default app