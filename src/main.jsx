import React from 'react'
import ReactMixin from 'react-mixin'
import ReactFireMixin from 'reactfire'
import { render } from 'react-dom'
import { Router, Route, browserHistory, IndexRoute, Link } from 'react-router'
import firebase from './start.js'

class App extends React.Component {
    componentWillMount() {
        const ref = firebase.database().ref("fruits")
        this.bindAsArray(ref, "fruits")
    }

    render () {
        return (
            <div>
                React-Firebase Quickstart
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li>
                        <Link to="/welcome/visitor">welcome</Link>
                    </li>
                </ul>
                { this.props.children }
                <h1>fruits</h1>
                <ul>
                    {
                        this.state.fruits.map((fruitsObject, index) => {
                            return <li key={index} >{fruitsObject['.value']}</li>
                        })
                    }
                </ul>
            </div>
        )
    }
}
ReactMixin(App.prototype, ReactFireMixin)

const Population = (props) => {
    return (
        <div>
            Population: 2
        </div>
    )
}

const WelcomeMessage = (props) => {
    return (
       <div>
            welcome { props.params.message }
       </div>
    )
}

const About = (props) => {
    return (
        <div>
            This is a city.
        </div>
    )
}

render((
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Population} />
            <Route path="/welcome/:message" component={WelcomeMessage} />
            <Route path="/about" component={About} />
        </Route>
    </Router>
), document.getElementById('app'))